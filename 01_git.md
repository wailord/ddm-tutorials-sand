---
marp: true
theme: uncover
---
# Git Tutorial
- Zpracoval Michal Bouška
- Dne 6.6.2020
- URL: https://wailord.gitlab.io/ddm-tutorials/01_git.html
---

# První kroky
Než začneme pracovat se samotným gitem, je potřeba udělat pár přípravných kroků. 
- registrace na gitlab
- vytvoření ssh klíče
    - linux
        ```
        ssh-keygen
        eval `ssh-agent`
        ssh-add
        ``` 
    - windows - no more putty - stejně jako u linuxu
- nahrání ssh klíče - setting - ssh keys - v linux formátu
---

# Co je to git
Git je nástroj pro spolupráci více lidí, nad textovými soubory (obvykle nad zdrojovými kódy), tak aby bylo vidět, kdo kdy co udělal.
Je navržený tak, aby ulehčoval práci ve více lidech, tedy hlavně následné dávání výsledků dohromady.

---

## Více formálně
V rámci gitu pracujeme v repozitáři. 
Repozitář se skládá z commitů, které reprezentují jednotlivé úpravy.
Jednotlivé commity jsou organizovány do větví, které znikají tím forkem a naopak mergem jsou změny sjednoceny.

---

# Vytvoření repozitáře
Využijeme již existující repozitář, který získáme příkazem `git clone git@gitlab.com:wailord/ddm-tutorials-sand.git`
- gitlab
    - Naklonování repozitáře `git clone url složka`
- lokálně
    - `git init složka`
    - `git remote add origin url`
---

# Základní info o repozitáři
Pro lepší přehled o tom jak repozitář vypadá a co se děje, slouží následující dva příkazy.
```
git log
git status
```
Popřípadě s parametry
```
git log --graph
git log --oneline
git log --graph --oneline
```
---

# Vytvoření větve
Začneme tím, že si vytvoříme větev(branch), tak abychom v ní mohli pracovat a nemuseli řešit kolize s ostatními. Naše větev bude na začátku shodná s větví ve které ji vytváříme.
```
git branch vetev
git checkout vetev
```
```
git checkout -b vetev
```
---

# Vytvoření souboru v gitu
Vytvoříme soubor ve větvi, upravíme ho, aby obsahoval námi požadovaný obsah a potého ho přidat do gitu pomocí příkazu `git add soubor`.
```
touch mujsoubor.txt # zvol si sve jmeno
# edituj ve svem oblibenem editoru
git add soubor
git add -p cesta
```
---

# Commit
Pokud již máme vytvořený soubor a je přidaný, můžeme provést commit, čímž se změny uloží do historie gitu, zatím jen lokálně.
```
git commit -m "jednoradkovy komentar"
git commit (pro víceřádkový komentář)
```
---

# Pull
Pokud chceme aktualizovat lokání obsah, použijeme příkaz pull. 
Defaultně se stahuje aktuální větev, vzhledem k tomu, že každý máme svou větev, neměli by při stahování ze serveru nastat žádné kolize.
```
git pull
git pull origin master (pro stažení větve master)
```
---

# Merge
Nyní je čas sloučit naše změny zase zpět do master větve a nahrát je na server.
```
git checkout master
git pull
git merge vetev
# v pripade ze merge neprobehl automaticky
git status
# opravit vsechny konfliktni soubory
# na vsechny opravene soubory zavolat git add
git pull
```
---

# Dobré zvyklosti
- Nikdy neupravuj master
- Každá větev odpovídá jedné přidané feature
- Každý commit by měl mít smyslupný komentář
- Nikdy necommituj to co nefunguje
- Existují speciální guidlines podle kterých vést repozitář - například [oneflow](https://www.endoflineblog.com/oneflow-a-git-branching-model-and-workflow)
- Buď připraven - [git-fire](https://github.com/qw3rtman/git-fire)
---

# Praktické cvičení
- každý by již měl mít alespoň jednu větev s jedním souborem, kterou mergnul do master větve
- nyní si každý vytvoří ještě jednu větev a upraví v ní alespoň dva soubory
- poprosím vás o chviličku strpení
- teď je cílem každého z vás nahrát své změny do master větve a brát přitom v úvahu změny kolegů
---

# Další materiály
- https://qeef.gitlab.io/talks/
---

# Gitlab pages
Podle dotazů publika, ústně

